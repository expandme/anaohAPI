<?php

class AssetCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('AssetController', true); // true means; LazyLoad
    $this->setPrefix('/assets');
    $this->get('/', 'all');
  }

}
