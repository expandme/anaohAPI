<?php

class JobCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('JobController', true); // true means; LazyLoad
    $this->setPrefix('/jobs');
    $this->get('/', 'all');
    $this->post('/', 'create');
    $this->post('/apply', 'applyJob');
    $this->put('/{job_id}', 'update');
    $this->get('/{job_id}', 'find');
    $this->delete('/{job_id}', 'remove');
  }

}
