<?php

class RoleTypeCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('RoleTypeController', true); // true means; LazyLoad
    $this->setPrefix('/role-type');
    $this->get('/{user_id}', 'getAssociatedUserRoles');
    $this->post('/add-user', 'addUser');
    $this->post('/generic', 'roleGeneric');
    $this->post('/actor', 'roleActor');
    $this->post('/producer', 'roleProducer');
    $this->post('/director', 'roleDirector');

  }

}
