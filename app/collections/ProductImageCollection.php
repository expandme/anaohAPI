<?php

class ProductImageCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('ProductImageController', true); // true means; LazyLoad
    $this->setPrefix('/product/images');
    $this->get('/', 'all');
    $this->post('/', 'create');
    $this->put('/{image_id}', 'update');
    $this->get('/{image_id}', 'find');
    $this->delete('/{image_id}', 'remove');
  }

}
