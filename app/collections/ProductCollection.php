<?php

class ProductCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('ProductController', true); // true means; LazyLoad
    $this->setPrefix('/products');
    $this->get('/', 'all');
    $this->post('/', 'create');
    $this->put('/{product_id}', 'update');
    $this->get('/{product_id}', 'find');
    $this->delete('/{product_id}', 'remove');
    $this->get('/in-progress', 'inProgress');
    $this->get('/is-author/{product_id}', 'isAuthor');
    $this->get('/connection/{product_id}', 'productConnection');
    $this->get('/connection-request/{product_id}', 'connectionRequest');
    $this->post('/recommendation', 'productRecommendation');
    $this->get('/author', 'projectsByAuthor');
    $this->post('/approve-connection', 'approveConnection');
    $this->post('/make-appointment', 'makeAppointment');
    $this->post('/make-investment', 'makeInvestment');

  }

}
