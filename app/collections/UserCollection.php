<?php

class UserCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('UserController', true);
    $this->setPrefix('/users');
    $this->get('/', 'listUser');
    $this->post('/', 'create');
    $this->get('/activate', 'activate');
    $this->get('/me', 'me');
    $this->post('/authenticate/{account}', 'authenticate');
    $this->get('/logout', 'logout');
    $this->get('/associated-user-role', 'associatedUserRoles');
    $this->get('/associated-user-product', 'associatedUserProduct');
    $this->get('/dashboard', 'authorDashboard');
    $this->get('/dashboard/{user_id}/{script_id}', 'manageScript');

  }
}
