<?php

class SearchCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('SearchController', true);
    $this->setPrefix('/search');
    $this->get('/', 'all');

  }
}
