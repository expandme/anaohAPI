<?php

class ProductVideoCollection extends \Phalcon\Mvc\Micro\Collection {

  public function __construct() {

    $this->setHandler('ProductVideoController', true); // true means; LazyLoad
    $this->setPrefix('/product/videos');
    $this->get('/', 'all');
    $this->post('/', 'create');
    $this->put('/{product_id}', 'update');
    $this->get('/{product_id}', 'find');
    $this->delete('/{product_id}', 'remove');
  }

}
