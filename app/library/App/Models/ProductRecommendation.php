<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class ProductRecommendation extends BaseModel {

  public $id;
  public $userId;
  public $productId;
  public $rating;
  public $summary;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'product_recommendation';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {
    return [
      'id' => 'id',
      'user_id' => 'userId',
      'product_id' => 'productId',
      'rating' => 'rating',
      'summary' => 'summary',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);

  }

}
