<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class ProductUserRole extends BaseModel {

  public $id;
  public $productId;
  public $userId;
  public $summary;
  public $permission;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'product_user_role';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'product_id' => 'productId',
      'user_id' => 'userId',
      'summary' => 'summary',
      'permission' => 'permission',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);
  }

}
