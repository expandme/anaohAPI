<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Jobs extends BaseModel {

  public $id;
  public $userId;
  public $title;
  public $description;
  public $summary;
  public $productId;
  public $jobTypeId;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'jobs';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'user_id' => 'userId',
      'title' => 'title',
      'description' => 'description',
      'summary' => 'summary',
      'product_id' => 'productId',
      'job_type_id' => 'jobTypeId',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function whitelist() {
    return [
      'title',
      'description',
      'summary',
      'productId'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);

    $this->belongsTo('jobTypeId', 'Library\App\Models\JobType', 'id', [
      'alias' => 'JobType',
    ]);

    $this->hasMany('id', 'Library\App\Models\JobsApplied', 'jobId', [
      'alias' => 'JobsApplied',
    ]);

  }

}
