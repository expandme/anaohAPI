<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class JobsApplied extends BaseModel {

  public $id;
  public $jobId;
  public $userId;
  public $productId;
  public $resume;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'jobs_applied';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'job_id' => 'jobId',
      'user_id' => 'userId',
      'product_id' => 'productId',
      'resume' => 'resume',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('jobId', 'Library\App\Models\Jobs', 'id', [
      'alias' => 'Jobs',
    ]);

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
        'alias' => 'Product',
    ]);
  }
}
