<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class WallPost extends BaseModel {

  public $id;
  public $userId;
  public $type;
  public $message;
  public $image;
  public $videoUrl;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'wall_post';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'user_id' => 'userId',
      'type' => 'type',
      'message' => 'message',
      'image' => 'image',
      'video_url' => 'videoUrl'
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

  }

}
