<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;
use Library\App\Models\Imageable as Imageable;

class ProductImage extends BaseModel {

  public $id;
  public $productId;
  public $uploadPath;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'product_image';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'product_id' => 'productId',
      'upload_path' => 'uploadPath',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'is_deleted' => 'is_deleted'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);

    /** @var \Phalcon\Config $config */
    $config = $this->getDI()->get('config');

    if (!isset($config->media) || !isset($config->media->upload_dir) || !is_string($config->media->upload_dir)) {
      throw new Exception(sprintf(
        "The config param %s is required and it must be string.",
        '\$config->media->upload_dir'
      ));
    }

    $this->uploadPath = rtrim($config->media->upload_dir, '/\\') . DIRECTORY_SEPARATOR .'product' . DIRECTORY_SEPARATOR . 'original';

    $this->thumbPath = rtrim($config->media->upload_dir, '/\\') . DIRECTORY_SEPARATOR .'product' . DIRECTORY_SEPARATOR . 'thumbnail';

    // NOTE: You need validate image (file size, file format, etc.)

    $this->addBehavior(new Imageable([
      'beforeCreate' => [
        'field'      => 'uploadPath',
        'uploadPath' => $this->uploadPath,
        'thumbPath'  => $this->thumbPath,
      ],
      'beforeUpdate' => [
        'field'      => 'uploadPath',
        'uploadPath' => $this->uploadPath,
        'thumbPath'  => $this->thumbPath,
      ],
    ]));
  }

  public function getImage() {

    return $this->image;
  }

  public function setImage($image) {

    $this->image = $image;

    return $this;
  }

}
