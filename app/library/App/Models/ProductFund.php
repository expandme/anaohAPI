<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class ProductFund extends BaseModel {

  public $id;
  public $productId;
  public $funderId;
  public $amount;
  public $remarks;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'product_fund';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'product_id' => 'productId',
      'funder_id' => 'funderId',
      'amount' => 'amount',
      'remarks' => 'remarks',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);

    $this->belongsTo('funderId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

  }

}
