<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;
use \PhalconRest\Exceptions\CoreException;

class UserRole extends BaseModel {

  public $id;
  public $userId;
  public $roleId;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'user_role';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'user_id' => 'userId',
      'role_id' => 'roleId',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

    $this->belongsTo('roleId', 'Library\App\Models\Role', 'id', [
      'alias' => 'Role',
    ]);

    $this->hasMany('id', 'Library\App\Models\JobsApplied', 'userRoleId', [
      'alias' => 'JobsApplied',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductUserRole', 'userRoleId', [
      'alias' => 'ProductUserRole',
    ]);
  }

}
