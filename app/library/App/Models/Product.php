<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Product extends BaseModel {

  public $id;
  public $title;
  public $shortDescription;
  public $summary;
  public $amount;
  public $userRoleId;
  public $ratings;
  public $lifecycle;
  public $coverImage;
  public $genreId;
  public $geoId;
  public $languageId;
  public $uniqueId;
  public $createdAt;
  public $updatedAt;
  public $is_deleted;

  protected $_rules;

  public function getSource() {

    return 'product';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'title' => 'title',
      'short_description' => 'shortDescription',
      'summary' => 'summary',
      'amount' => 'amount',
      'user_role_id' => 'userRoleId',
      'ratings' => 'ratings',
      'lifecycle' => 'lifecycle',
      'cover_image' => 'coverImage',
      'genre_id' => 'genreId',
      'geo_id' => 'geoId',
      'language_id' => 'languageId',
      'unique_id' => 'uniqueId',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'is_deleted' => 'is_deleted'
    ];
  }

  public function whitelist() {
    return [
      'title',
      'shortDescription',
      'summary',
      'amount',
      'userRoleId',
      'genreId',
      'geoId',
      'languageId'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasMany('id', 'Library\App\Models\ProductRole', 'productId',[
      'alias' => 'ProductRole',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductImage', 'productId',[
      'alias' => 'ProductImage',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductVideo', 'productId',[
      'alias' => 'ProductVideo',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductUserRole', 'productId',[
      'alias' => 'ProductUserRole',
    ]);

    $this->hasMany('id', 'Library\App\Models\Jobs', 'productId',[
      'alias' => 'Jobs',
    ]);

    $this->hasMany('id', 'Library\App\Models\Appointment', 'productId',[
      'alias' => 'Appointment',
    ]);

    //Multiple Investors are allowed to fund for a single script
    $this->hasMany('id', 'Library\App\Models\ProductFund', 'productId',[
      'alias' => 'ProductFund',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductRecommendation', 'productId',[
      'alias' => 'ProductRecommendation',
    ]);

    $this->hasMany('id', 'Library\App\Models\JobsApplied', 'productId', [
        'alias' => 'JobsApplied',
    ]);

    $this->belongsTo('userRoleId', 'Library\App\Models\UserRole', 'id', [
      'alias' => 'UserRole',
    ]);

    $this->belongsTo('genreId', 'Library\App\Models\Genre', 'id', [
      'alias' => 'Genre',
    ]);

    $this->belongsTo('geoId', 'Library\App\Models\Geo', 'id', [
      'alias' => 'Geo',
    ]);

    $this->belongsTo('languageId', 'Library\App\Models\Language', 'id', [
      'alias' => 'Language',
    ]);
  }

  public static function getProductDetails() {

  }

}
