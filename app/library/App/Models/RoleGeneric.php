<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class RoleGeneric extends BaseModel {

  public $id;
  public $name;
  public $phone;
  public $email;
  public $url;
  public $biography;
  public $reference;
  public $keywords;
  public $createdAt;
  public $updatedAt;
  public $is_deleted;

  public function getSource() {

    return 'role_generic';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'designation' => 'designation',
      'phone' => 'phone',
      'email' => 'email',
      'url' => 'url',
      'biography' => 'biography',
      'reference' => 'reference',
      'keywords' => 'keywords',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'is_deleted' => 'is_deleted',
    ];
  }

  public function initialize() {

    parent::initialize();
  }
}
