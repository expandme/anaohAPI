<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Role extends BaseModel {

  public $id;
  public $name;
  public $template;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'role';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'template' => 'template',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasManyToMany('id', 'Library\App\Models\UserRole', 'roleId', 'userId', 'Library\App\Models\User', 'id', [
      'alias' => 'Users',
    ]);
  }

}
