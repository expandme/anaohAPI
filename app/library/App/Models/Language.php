<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Language extends BaseModel {

  public $id;
  public $name;
  public $short;

  public function getSource() {

    return 'language';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'short' => 'short'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasMany('id', 'Library\App\Models\Product', 'languageId', [
      'alias' => 'Product',
    ]);
  }

}
