<?php

namespace Library\App\Models;
use \Phalcon\Mvc\Model as Model;
use \PhalconRest\Constants\ErrorCodes as ErrorCodes;
use \PhalconRest\Exceptions\CoreException;
use \PhalconRest\Exceptions\UserException;
use \PhalconRest\Validation\Validator;
use Phalcon\Mvc\Model\Behavior\SoftDelete as SoftDelete;

class BaseModel extends Model {

  protected $validator;

  public $is_deleted;

  const DELETED = 1;

  const NOT_DELETED = 0;

  public function initialize() {

    $this->addBehavior(new SoftDelete(
      array(
        'field' => 'is_deleted',
        'value' => self::DELETED
      )
    ));
  }

  public function beforeValidationOnCreate() {
    // The record is not soft deleted by default
    $this->is_deleted = self::NOT_DELETED;

    $this->createdAt = date('Y-m-d H:i:s');
    $this->updatedAt = date('Y-m-d H:i:s');
  }

  public function beforeValidationOnUpdate() {
    $this->updatedAt = date('Y-m-d H:i:s');
  }

  public function getValidator() {
    if (!$this->validator) {

      $this->validator = Validator::make($this, $this->validateRules());
    }

    return $this->validator;
  }

  public function validation() {
    if (!method_exists($this, 'validateRules')) {
      return true;
    }

    $this->validator = $this->getValidator();

    return $this->validator->passes();
  }

  public function onValidationFails() {
    $message = null;
    if ($this->validator) {
      $message = $this->validator->getFirstMessage();
    }

    if ($messages = $this->getMessages()) {
      $message = $messages[0]->getMessage();
    }

    if (is_null($message)) {

      $message = 'Could not validate data';
    }

    throw new UserException(ErrorCodes::DATA_INVALID, $message);
  }

  public function prepare($data) {

    $whitelist = $this->whitelist();

    foreach ($whitelist as $field) {
      if (isset($data->$field)) {
        $this->$field = $data->$field;
      }
    }
  }

  public static function createFrom($data) {
    $modelName = get_called_class();
    $modelInstance = new $modelName;

    if (!isset($modelInstance->_whitelist)) {
      throw new CoreException('No whitelist declared for: ' . $modelName);
    }
  }

  public static function exists($id) {
    return self::findFirstById($id);
  }

  public static function remove($opts) {
    $result = self::findFirstById($opts);

    if (!$result) {
      return false;
    }

    return $result->delete();
  }


  public static function find($parameters = null) {

    $parameters = self::softDeleteFetch($parameters);
    return parent::find($parameters);
  }

  public static function findFirst($parameters = null) {

    $parameters = self::softDeleteFetch($parameters);
    return parent::findFirst($parameters);
  }

  public static function count($parameters = null) {

    $parameters = self::softDeleteFetch($parameters);
    return parent::count($parameters);
  }

  public static function softDeleteFetch($parameters = null) {

    if (method_exists(get_called_class(), 'getDeleted') === false) {
      return $parameters;
    }

    $deletedField = call_user_func([get_called_class(), 'getDeleted']);

    if ($parameters === null) {
      $parameters = $deletedField . ' = 0';
    } elseif (
      is_array($parameters) === false &&
      strpos($parameters, $deletedField) === false
    ) {
      $parameters .= ' AND ' . $deletedField . ' = 0';
    } elseif (is_array($parameters) === true) {
      if (
        isset($parameters[0]) === true &&
        strpos($parameters[0], $deletedField) === false
      ) {
        $parameters[0] .= ' AND ' . $deletedField . ' = 0';
      } elseif (
        isset($parameters['conditions']) === true &&
        strpos($parameters['conditions'], $deletedField) === false
      ) {
        $parameters['conditions'] .= ' AND ' . $deletedField . ' = 0';
      } else {
      $parameters['conditions'] = " $deletedField = 0 ";
      }
    }

    return $parameters;
  }

}
