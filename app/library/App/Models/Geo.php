<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Geo extends BaseModel {

  public $id;
  public $name;
  public $locationType;
  public $parentId;
  public $is_deleted;

  public function getSource() {

    return 'geo';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'location_type' => 'locationType',
      'parent_id' => 'parentId',
      'is_deleted' => 'is_deleted'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasMany('id', 'Library\App\Models\Product', 'geoId', [
      'alias' => 'Product',
    ]);

    $this->hasMany('id', 'Library\App\Models\User', 'geoId', [
      'alias' => 'User',
    ]);
  }

}
