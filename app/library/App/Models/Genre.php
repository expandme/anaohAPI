<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Genre extends BaseModel {

  public $id;
  public $name;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'genre';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasMany('id', 'Library\App\Models\Product', 'genreId', [
      'alias' => 'Product',
    ]);
  }

}
