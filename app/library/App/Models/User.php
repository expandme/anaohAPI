<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;
use \PhalconRest\Exceptions\CoreException;

class User extends BaseModel {

  public $id;
  public $name;
  public $email;
  public $phone;
  public $address;
  public $geoId;
  public $postalCode;
  public $active;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;
  public $mailToken;

  public function getSource() {

    return 'user';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {
    return [
      'id' => 'id',
      'name' => 'name',
      'email' => 'email',
      'phone' => 'phone',
      'address' => 'address',
      'geo_id'  => 'geoId',
      'postal_code' => 'postalCode',
      'is_active' => 'active',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'mail_token' => 'mailToken'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->hasOne('id', 'Library\App\Models\UsernameAccount', 'userId', [
      'alias' => 'UsernameAccount',
    ]);

    $this->hasMany('id', 'Library\App\Models\UserRole', 'userId', [
      'alias' => 'UserRole',
    ]);

    $this->hasMany('id', 'Library\App\Models\Jobs', 'userId', [
      'alias' => 'Jobs',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductUserRole', 'userId', [
      'alias' => 'ProductUserRole',
    ]);

    $this->hasMany('id', 'Library\App\Models\WallPost', 'userId', [
      'alias' => 'WallPost',
    ]);

    $this->hasMany('id', 'Library\App\Models\Appointment', 'userId', [
      'alias' => 'Appointment',
    ]);
    // $this->hasManyToMany('id', 'Library\App\Models\UserRole', 'userId', 'roleId', 'Library\App\Models\Role', 'id', [
    //   'alias' => 'UserRole',
    // ]);

    $this->hasMany('id', 'Library\App\Models\ProductFund', 'funderId', [
      'alias' => 'ProductFund',
    ]);

    $this->hasMany('id', 'Library\App\Models\ProductRecommendation', 'userId', [
      'alias' => 'ProductRecommendation',
    ]);

    $this->hasMany('id', 'Library\App\Models\JobsApplied', 'userId', [
        'alias' => 'JobsApplied',
    ]);

    $this->belongsTo('geoId', 'Library\App\Models\Geo', 'id', [
      'alias' => 'Geo',
    ]);
  }

  public function validateRules() {

    return [
      'name' => 'pattern:/[A-Za-z ]{0,55}/', // should contain between 0 - 55 letters
      'email' => 'email', // should be an email address
      'phone' => 'phone'
    ];
  }

  public function getAccount($account) {

    if ($account === \Library\App\Constants\AccountTypes::USERNAME) {
      return $this->usernameAccount;
    }

    return false;
  }

  public function getByUsername($username) {

    return UsernameAccount::findFirstByUsername($username);
  }
}
