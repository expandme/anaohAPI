<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class ProductVideo extends BaseModel {

  public $id;
  public $productId;
  public $url;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'product_video';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'product_id' => 'productId',
      'url' => 'url',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'is_deleted' => 'is_deleted'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);
  }

}
