<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class Appointment extends BaseModel {

  public $id;
  public $userId;
  public $productId;
  public $date;
  public $remarks;
  public $is_deleted;
  public $createdAt;
  public $updatedAt;

  public function getSource() {

    return 'appointment';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'user_id' => 'userId',
      'product_id' => 'productId',
      'date' => 'date',
      'remarks' => 'remarks',
      'is_deleted' => 'is_deleted',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt'
    ];
  }

  public function initialize() {

    parent::initialize();

    $this->belongsTo('userId', 'Library\App\Models\User', 'id', [
      'alias' => 'User',
    ]);

    $this->belongsTo('productId', 'Library\App\Models\Product', 'id', [
      'alias' => 'Product',
    ]);
  }

}
