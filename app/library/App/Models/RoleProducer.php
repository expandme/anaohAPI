<?php

namespace Library\App\Models;
use Library\App\Models\BaseModel as BaseModel;

class RoleProducer extends BaseModel {

  public $id;
  public $name;
  public $phone;
  public $email;
  public $url;
  public $companyName;
  public $summary;
  public $keywords;
  public $createdAt;
  public $updatedAt;
  public $is_deleted;

  public function getSource() {

    return 'role_producer';
  }

  public static function getDeleted() {

    return 'is_deleted';
  }

  public function columnMap() {

    return [
      'id' => 'id',
      'name' => 'name',
      'phone' => 'phone',
      'email' => 'email',
      'url' => 'url',
      'company_name' => 'companyName',
      'summary' => 'summary',
      'keywords' => 'keywords',
      'created_on' => 'createdAt',
      'updated_on' => 'updatedAt',
      'is_deleted' => 'is_deleted',
    ];
  }

  public function initialize() {

    parent::initialize();
  }
}
