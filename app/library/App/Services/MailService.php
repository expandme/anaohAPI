<?php

namespace Library\App\Services;

use Library\App\Constants\Services as AppServices;
use \PhalconRest\Constants\Services as PhalconRestServices;

class MailService extends \Phalcon\Mvc\User\Plugin {

  /**
   * Send activation mail to user mail id
   *
   * @param $user
   * @param $account
   * @return mixed
   */
  public function sendActivationMail($user, $account) {

    $config = $this->di->get(AppServices::CONFIG);
    $view = $this->di->get(AppServices::VIEW);
    $mailer = $this->di->get(PhalconRestServices::MAILER);
    $link = $config->clientHostName . 'users/activate?mailtoken=' . $user->mailToken;

    $mailer->setSubject($config->activationMail->subject);
    $mailer->addAddress($user->email, $user->name);

    // Render mail template
    $view->setVar('link', $link);
    $view->setVar('user', $user);
    $view->setVar('account', $account);
    $renderedView = $view->render($config->activationMail->template);

    // Add template to mail body
    $mailer->setHtmlBody($renderedView);

    return $mailer->send();
  }

  /**
   * Send a email confirmation to user for appointment
   *
   * @param $user
   * @param $appointment
   * @return mixed
   */
  public function sendAppointmentMail($user, $appointment) {

    $config = $this->di->get(AppServices::CONFIG);
    $view = $this->di->get(AppServices::VIEW);
    $mailer = $this->di->get(PhalconRestServices::MAILER);

    $mailer->setSubject($config->appointmentMail->subject);
    $mailer->addAddress($user->email, $user->name);

    $view->setVar('user', $user);
    $view->setVar('appointment', $appointment);

    $renderedView = $view->render($config->appointmentMail->template);

    // Add template to mail body
    $mailer->setHtmlBody($renderedView);

    return $mailer->send();
  }

  /**
   * Mail for forget password
   *
   * @param $user
   */
  public function sendForgetPasswordMail($user) {

  }
}
