<?php

/**
 * Here we mount our collections
 */
$app->mount(new ProductCollection);
$app->mount(new ProductImageCollection);
$app->mount(new ProductVideoCollection);
$app->mount(new UserCollection);
$app->mount(new JobCollection);
$app->mount(new AssetCollection);
$app->mount(new RoleTypeCollection);
$app->mount(new SearchCollection);



