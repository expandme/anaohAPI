<?php

use League\Fractal;

class JobTransformer extends Fractal\TransformerAbstract {

  /**
   * Turn this resource object into a generic array
   *
   * @return array
   */
  public function transform($jobs) {

    return [
      'id' => (int) $jobs->id,
      'userId' => (int) $jobs->userId,
      'title' => $jobs->title,
      'description' => $jobs->description,
      'summary' => $jobs->summary,
      'createdAt' => date('M j Y g:i A', strtotime($jobs->createdAt)),
      'updatedAt' => date('M j Y g:i A', strtotime($jobs->updatedAt)),
      'is_deleted' => (int) $jobs->is_deleted,
    ];
  }
}
