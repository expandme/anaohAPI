<?php

use League\Fractal;

class ProductImageTransformer extends Fractal\TransformerAbstract {

  /**
   * Turn this resource object into a generic array
   *
   * @return array
   */
  public function transform($image) {

    return [
      'id' => (int) $image->id,
      'productId' => (int) $image->productId,
      'uploadPath' => $image->uploadPath,
      'createdAt' => (int) strtotime($image->createdAt) * 1000,
      'updatedAt' => (int) strtotime($image->updatedAt) * 1000,
      'is_deleted' => (int) $image->is_deleted,
    ];
  }
}
