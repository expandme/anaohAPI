<?php

use League\Fractal;

class AccountTransformer extends Fractal\TransformerAbstract {

  protected $defaultIncludes = [
    'username'
  ];

  public function transform($user) {

    // Only allow include when data present
    $this->defaultIncludes = [];

    // Include when username account is present
    if($user->usernameAccount) {
      $this->defaultIncludes[] = 'username';
    }

    // Only include accounts
    return [];
  }

  public function includeUsername($user) {

    return $this->item($user->usernameAccount, new UsernameAccountTransformer, 'parent');
  }
}
