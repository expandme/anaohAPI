<?php

use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract {

  protected $availableIncludes = [
    'accounts',
  ];

  public function transform($user) {

    return [
      'id' => (int) $user->id,
      'name' => $user->name,
      'email' => $user->email,
      'phone' => $user->phone,
      'address' => $user->address,
      'geoId' => (int) $user->geoId,
      'postalCode' => $user->postalCode,
      'active' => (int) $user->active,
      'createdAt' => (int) strtotime($user->createdAt) * 1000,
      'updatedAt' => (int) strtotime($user->updatedAt) * 1000,
      'is_deleted' => (int) $user->is_deleted,
      'dateRegistered' => (int) strtotime($user->createdAt) * 1000
    ];
  }

  public function includeAccounts($user) {

    return $this->item($user, new AccountTransformer, 'parent');
  }
}
