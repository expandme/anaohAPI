<?php

use League\Fractal;

class ProductVideoTransformer extends Fractal\TransformerAbstract {

  /**
   * Turn this resource object into a generic array
   *
   * @return array
   */
  public function transform($video) {

    return [
      'id' => (int) $video->id,
      'productId' => (int) $video->productId,
      'url' => $video->url,
      'createdAt' => (int) strtotime($video->createdAt) * 1000,
      'updatedAt' => (int) strtotime($video->updatedAt) * 1000,
      'is_deleted' => (int) $video->is_deleted,
    ];
  }
}
