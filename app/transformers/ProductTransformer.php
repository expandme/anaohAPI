<?php

use League\Fractal;

class ProductTransformer extends Fractal\TransformerAbstract {

  /**
   * Turn this resource object into a generic array
   *
   * @return array
   */
  public function transform($product) {

    return [
      'id' => (int) $product->id,
      'title' => $product->title,
      'shortDescription' => $product->shortDescription,
      'summary' => $product->summary,
      'amount' => (int) $product->amount,
      'userRoleId' => (int) $product->userRoleId,
      'ratings' => (int) $product->ratings,
      'lifecycle' => (int) $product->lifecycle,
      'coverImage' => $product->coverImage,
      'genreId' => (int) $product->genreId,
      'geoId' => (int) $product->geoId,
      'languageId' => (int) $product->languageId,
      'createdAt' => (int) strtotime($product->createdAt) * 1000,
      'updatedAt' => (int) strtotime($product->updatedAt) * 1000,
      'is_deleted' => (int) $product->is_deleted,
    ];
  }
}
