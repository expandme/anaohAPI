<?php

use Library\App\Models\Geo as Geo;
use Library\App\Models\Genre as Genre;
use Library\App\Models\Language as Language;
use Library\App\Models\Role as Role;
use Library\App\Models\JobType as JobType;

class AssetController extends \PhalconRest\Mvc\Controller {

  public function all() {

    $query  = $this->request->getQuery();
    $namespace = "Library\App\Models";

    $output = array();
    foreach ($query['resource'] as $value) {
      $model = $namespace."\\".$value;
      $data = $model::find()->toArray();
      $output[$value] = $data;
    }
    return $output;
  }
}
