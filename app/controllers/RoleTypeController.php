<?php

use Library\App\Models\User as User;
use Library\App\Models\UserRole as UserRole;
use Library\App\Models\Role as Role;
use Library\App\Models\ProductRole;
use Library\App\Models\RoleActor as RoleActor;
use Library\App\Models\RoleDirector as RoleDirector;
use Library\App\Models\RoleGeneric as RoleGeneric;
use Library\App\Models\RoleProducer as RoleProducer;

class RoleTypeController extends \PhalconRest\Mvc\Controller
{

    public function roleGeneric()
    {

    }

    public function getAssociatedUserRoles($id)
    {
        $params = [
            'columns' => 'id, roleId',
            'conditions' => 'userId=?1',
            'bind' => [
                1 => $id
            ]
        ];

        $user_roles = UserRole::find($params);

        $userRoleDetails = [];
        $roles = [];
        foreach ($user_roles as $user_role) {
            $role = Role::findFirstById($user_role->roleId);
            $userRoleDetails[] = [
                'userRoleId' => $user_role->id,
                'roleName' => $role->name,
            ];

            array_push($roles, $role->id);
        }

        return ['userRoleDetails' => $userRoleDetails, 'associatedRoles' => $roles];
    }

    public function addUser()
    {


    }

}
