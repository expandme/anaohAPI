<?php

use Library\App\Models\ProductImage as ProductImage;
use \PhalconRest\Constants\ErrorCodes as ErrorCodes;
use \PhalconRest\Exceptions\UserException;

/**
 * @resource("Product Images")
 */

 class ProductImageController extends PhalconRest\Mvc\Controller {

  /**
   * @title("All")
   * @description("Get all products")
   * @response("Collection of Product images objects or Error object")
   * @requestExample("GET /products")
   */
  public function all() {

    $images = ProductImage::find();

    return $this->createCollection($images, new ProductImageTransformer, 'images');
  }

  /**
   * @title("Find")
   * @response("Product object or Error object")
   * @requestExample("GET /product/images/14")
   *
   */
  public function find($product_image_id) {

    $product_image = ProductImage::findFirstById($product_image_id);

    if(!$product_image) {

      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Product with image id: #' . $product_image_id . ' could not be found.');
    }

    return $this->createItem($product_image, new ProductImageTransformer, 'image');
  }

  /**
   * @title("Create")
   * @description("Create a new product")
   * @response("Product object or Error object")
   */
  public function create() {

    if($this->request->hasFiles() == true && $this->request->getPost('productId')) {

      $product_image = new ProductImage;
      $product_image->productId = $this->request->getPost('productId');
      $product_image->setImage($this->request->getUploadedFiles());

      if (!$product_image->save()) {

        throw new UserException(ErrorCodes::DATA_FAIL, 'Could not create product image.');
      }

      return $this->createItemWithOK($product_image, new ProductImageTransformer, 'image');

    }

  }

  /**
   * @title("Update")
   * @description("Update a product image")
   * @response("Product Image object or Error object")
   */
  public function update($product_image_id) {

    $product_image = ProductImage::findFirstById($product_image_id);

    if(!$product_image) {

      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Could not find product.');
    }

    $data = $this->request->getJsonRawBody();

    // Prepare method is provided by \OA\Phalcon\Mvc\Model
    $product_image->prepare($data);

    if(!$product_image->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not update product.');
    }

    return $this->createItemWithOK($product_image, new ProductImageTransformer, 'product');
  }

  /**
   * @title("Remove")
   * @description("Remove a product image by id")
   * @response("Result object or Error object")
   * @responseExample({
   *     "result": "OK"
   * })
   */
  public function remove($product_image_id) {

    if(!ProductImage::remove($product_image_id)) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not remove product.');
    }

    return $this->respondWithOK();
  }

 }
