<?php

use Library\App\Models\Jobs as Jobs;
use Library\App\Models\JobsApplied as JobsApplied;
use \PhalconRest\Constants\ErrorCodes as ErrorCodes;
use \PhalconRest\Exceptions\UserException;
use Library\App\Constants\Services as AppServices;

/**
 * @resource("Job")
 */

class JobController extends PhalconRest\Mvc\Controller {

  public function onConstruct() {

    parent::onConstruct();
    $this->userService = $this->di->get(AppServices::USER_SERVICE);
  }

  /**
   * @title("All")
   * @description("Get all jobs")
   * @response("Collection of job objects or Error object")
   * @requestExample("GET /jobs")
   */
  public function all() {

    $limit = $this->request->getQuery('limit', 'int');
    $offset = $this->request->getQuery('offset', 'int');
    $data = [];

    $jobs = Jobs::find(array('limit' => $limit, 'offset' => $offset));

    foreach($jobs as $job) {
      $data[] = [
        'id' => $job->id,
        'title' => $job->title,
        'summary' => $job->summary,
        'author' => $job->User->name,
        'authorId' => $job->User->id,
        'postedOn' => date('M j Y', strtotime($job->createdAt)),
        'jobType' => $job->JobType->name,
        'scriptId' => $job->Product->id,
        'scriptTitle' => $job->Product->title,
        'location' => $job->Product->Geo->name
      ];
    }

    return $data;
    //return $this->createCollection($jobs, new JobTransformer, 'jobs');
  }


  /**
   * @title("Find")
   * @description("Get all jobs")
   * @response("Job object or Error object")
   * @requestExample("GET /jobs/14")
   */
  public function find($job_id) {

    $job = Jobs::findFirstById($job_id);

    if(!$job) {
      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Job with id: #' . $job_id . ' could not be found.');
    }

    $data = [
      'id' => $job->id,
      'title' => $job->title,
      'summary' => $job->summary,
      'description' => $job->description,
      'author' => $job->User->name,
      'authorId' => $job->User->id,
      'postedOn' => date('M j Y', strtotime($job->createdAt)),
      'jobType' => $job->JobType->name,
      'scriptId' => $job->Product->id,
      'scriptTitle' => $job->Product->title,
      'location' => $job->Product->Geo->name
    ];

    return $data;
    //return $this->createItem($job, new JobTransformer, 'job');
  }


  /**
   * @title("Create")
   * @description("Create a new job")
   * @response("Job object or Error object")
   */
  public function create() {

    $user = $this->userService->me();

    $data = $this->request->getJsonRawBody();

    $job = new Jobs;

    $job->userId = $user->id;
    $job->title = $data->title;
    $job->description = $data->description;
    $job->summary = $data->summary;
    $job->productId = $data->script->id;
    $job->jobTypeId = $data->type->id;

    if (!$job->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not create job.');
    }

    return $this->createItemWithOK($job, new JobTransformer, 'job');
  }

  /**
   * @title("Update")
   * @description("Update a job")
   * @response("Job object or Error object")
   */
  public function update($job_id) {

    $job = Jobs::findFirstById($job_id);

    if(!$job) {

      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Could not find job.');
    }

    $data = $this->request->getJsonRawBody();

    // Prepare method is provided by \OA\Phalcon\Mvc\Model
    $job->prepare($data);

    if(!$job->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not update job.');
    }

    return $this->createItemWithOK($job, new JobTransformer, 'job');
  }

  /**
   * @title("Remove")
   * @description("Remove a job")
   * @response("Result object or Error object")
   * @responseExample({
   *     "result": "OK"
   * })
   */
  public function remove($job_id) {

    if(!Jobs::remove($job_id)) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not remove job.');
    }

    return $this->respondWithOK();
  }

  public function applyJob() {
    $user = $this->userService->me();
    $data = $this->request->getJsonRawBody();

    $job = new JobsApplied;

    $job->userId = $user->id;
    $job->jobId = $data->jobId;
    $job->productId = $data->productId;
    $job->resume = $data->resume;

    if (!$job->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not apply job.');
    }

    return ['status' => 'OK', 'message' => 'job applied successfully'];

  }
}
