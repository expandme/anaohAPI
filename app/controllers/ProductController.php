<?php

use Library\App\Models\Product as Product;
use Library\App\Models\ProductUserRole as ProductUserRole;
use Library\App\Models\ProductImage as ProductImage;
use Library\App\Models\UserRole as UserRole;
use Library\App\Models\User as User;
use Library\App\Models\Geo as Geo;
use Library\App\Models\Genre as Genre;
use Library\App\Models\Language as Language;
use Library\App\Models\Jobs as Jobs;
use Library\App\Models\ProductFund as ProductFund;
use Library\App\Models\Appointment as Appointment;
use Library\App\Models\ProductRecommendation as ProductRecommendation;
use \PhalconRest\Constants\ErrorCodes as ErrorCodes;
use \PhalconRest\Exceptions\UserException;
use Library\App\Constants\Services as AppServices;

/**
 * @resource("Product")
 */

 class ProductController extends PhalconRest\Mvc\Controller {

   public function onConstruct() {

     parent::onConstruct();
     $this->userService = $this->di->get(AppServices::USER_SERVICE);
   }

  /**
   * @title("All")
   * @description("Get all products")
   * @response("Collection of Product objects or Error object")
   * @requestExample("GET /products")
   */
  public function all() {

    $limit = $this->request->getQuery('limit', 'int');
    $offset = $this->request->getQuery('offset', 'int');
    $output = [];
    $products = Product::find(array('limit' => $limit, 'offset' => $offset));

    foreach($products as $product) {
      $paramsInvestors = [
        'columns' => 'id',
        'conditions' => 'productId=?1',
        'bind' => [
          1 => $product->id
        ]
      ];

      $paramsRecommendation = [
        'columns' => 'id',
        'conditions' => 'productId=?1',
        'bind' => [
          1 => $product->id
        ]
      ];

      $paramsConnection = [
        'columns' => 'id',
        'conditions' => 'productId=?1 AND permission=1',
        'bind' => [
          1 => $product->id
        ]
      ];
      $investors = ProductFund::find($paramsInvestors);
      $recommendation = ProductRecommendation::find($paramsRecommendation);
      $productConnections = ProductUserRole::find($paramsConnection);
      $output[] = [
        'productId' => $product->id,
        'title' => $product->title,
        'summary' => $product->summary,
        'shortDescription' => $product->shortDescription,
        'funding' => $product->amount,
        'coverImage' => $product->coverImage,
        'location' => $product->Geo->name,
        'language' => $product->Language->name,
        'creatorName' => $product->UserRole->User->name,
        'creatorId' => $product->UserRole->User->id,
        'investors' => count($investors),
        'recommendations' => count($recommendation),
        'connections' => count($productConnections)
      ];
    }

    return ['products' => $output];

  }

  public function inProgress($limit = null) {

    $product = Product::find(
      array(
        "lifecycle = 0",
        "limit" => $limit
        )
      );

    return $product->toArray();
  }

  public function isAuthor($product_id) {

    $author = new UserController();
    $author = $author->me();
    $userCondition = [
      'columns' => 'id',
      'conditions' => 'userId=?1',
      'bind' => [
        1 => $author['userDetails']->id
      ]
    ];

    $userRoles = UserRole::find($userCondition);
    $count = 0;
    $isAuthor = false;
    foreach ($userRoles as $userRole) {
      $authorCondition = [
        'columns' => 'id',
        'conditions' => 'id=?1 AND userRoleId=?2',
        'bind' => [
          1 => $product_id,
          2 => $userRole->id
        ]
      ];

      $productDetails = Product::find($authorCondition);
      if(!empty($productDetails->toArray())) {
        $count++;
      }
    }

    if($count > 0) $isAuthor = true;
    return ['isAuthor' => $isAuthor];
  }

  public function productConnection($product_id) {
    $user = new UserController();
    $user = $user->me();
    $param = [
      'conditions' => 'productId=?1 AND userId=?2',
      'bind' => [
        1 => $product_id,
        2 => $user['userDetails']->id
      ]
    ];

    $result = ProductUserRole::find($param);

    if(count($result) > 0) {
      return ['status' => 'error', 'message' => 'connection request already sent'];
    } else {
      $connection = new ProductUserRole;
      $connection->productId = $product_id;
      $connection->userId = $user['userDetails']->id;
      $connection->permission = 0;
      if($connection->save()) return ['status' => 'success', 'message' => 'connection request sent'];
    }
  }

  public function productRecommendation() {
    $data = $this->request->getJsonRawBody();
    $user = new UserController();
    $user = $user->me();
    $recommendation = new ProductRecommendation;
    $recommendation->userId = $user['userDetails']->id;
    $recommendation->productId = $data->scriptId;
    $recommendation->rating = $data->ratingValue;
    $recommendation->summary = $data->comment;
    if(!$recommendation->save()) return ['status' => 'error', 'message' => 'some error occured'];
    else return ['status' => 'success', 'message' => 'Thanks! recommendation submitted'];
  }

  public function approveConnection() {

    $data = $this->request->getJsonRawBody();

    $row = ProductUserRole::findFirstById($data->id);

    if(!$row) {

      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Could not find connection.');
    }

    $row->permission = 1;

    if(!$row->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not update connection.');
    }

    return ['status' => 'success', 'message' => 'connection approved'];
  }

  public function connectionRequest($product_id) {

    $connectionRequests = [];
    $param = [
      'conditions' => 'productId=?1 AND permission = 0',
      'bind' => [
        1 => $product_id
      ]
    ];

    $results = ProductUserRole::find($param);
    foreach($results as $connection) {
      $connectionRequests[] = [
        'id' => $connection->id,
        'user' => $connection->User->name
      ];
    }

    return ['list' => $connectionRequests, 'count' => count($results)];
  }


  public function makeAppointment() {
    $data = $this->request->getJsonRawBody();
    $user = new UserController();
    $user = $user->me();
    $appointment = new Appointment;
    $appointment->userId = $user['userDetails']->id;
    $appointment->productId = $data->scriptId;
    $appointment->date = $data->date;
    $appointment->remarks = $data->remarks;
    if(!$appointment->save()) return ['status' => 'error', 'message' => 'some error occured'];
    else {
      $this->userService->sendAppointmentMail($user['userDetails'], $appointment);
      return ['status' => 'success', 'message' => 'Thanks! appointment booked.kindly check your mail. we will contact you soon'];
    }
  }

  public function makeInvestment() {
    $data = $this->request->getJsonRawBody();
    $user = new UserController();
    $user = $user->me();
    $funding = new ProductFund;
    $funding->productId = $data->scriptId;
    $funding->funderId = $user['userDetails']->id;
    $funding->amount = $data->amount;
    $funding->remarks = $data->remarks;
    if(!$funding->save()) return ['status' => 'error', 'message' => 'some error occured'];
    else return ['status' => 'success', 'message' => 'Thanks for your investment!'];
  }

  /**
   * @title("Find")
   * @description("Get all products")
   * @response("Product object or Error object")
   * @requestExample("GET /product/14")
   *
   */
  public function find($product_id) {

    $product = Product::findFirstById($product_id);
    $productDetails = [];
    $connections = [];
    $recommendations = [];
    $investors = [];
    $userCondition = [
        'columns' => 'id, productId, userId, summary',
        'conditions' => 'productId=?1 AND permission = 1',
        'bind' => [
          1 => $product_id
        ]
      ];

    $fundCondition = [
        'columns' => 'id, productId, funderId',
        'conditions' => 'productId=?1',
        'bind' => [
          1 => $product_id
        ]
      ];

    $jobCondition = [
        'columns' => 'id, title, description, summary',
        'conditions' => 'productId=?1',
        'bind' => [
          1 => $product_id
        ]
      ];

    $recommendCondition = [
        'conditions' => 'productId=?1',
        'bind' => [
          1 => $product_id
        ]
      ];

    $productConnections = ProductUserRole::find($userCondition);
    $jobs = Jobs::find($jobCondition);
    $productInvestors = ProductFund::find($fundCondition);
    $ProductRecommendations = ProductRecommendation::find($recommendCondition);

    foreach ($productInvestors as $productInvestor) {
      $user = User::findFirstById($productInvestor->funderId);
      $investors[] = [
        'id' => $user->id,
        'name' => $user->name,
        'location' => $user->Geo->name
      ];
    }

    foreach ($productConnections as $productConnection) {
      $user = User::findFirstById($productConnection->userId);
      $connections[] = [
        'id' => $user->id,
        'name' => $user->name,
        'email' => $user->email,
      ];
    }

    foreach($ProductRecommendations as $ProductRecommendation) {
      $recommendations[] = [
        'userId' => $ProductRecommendation->User->id,
        'userName' => $ProductRecommendation->User->name,
        'rating' => $ProductRecommendation->rating,
        'summary' => $ProductRecommendation->summary
      ];
    }

    $productDetails = [
      'productId' => $product->id,
      'title' => $product->title,
      'summary' => $product->summary,
      'shortDescription' => $product->shortDescription,
      'funding' => $product->amount,
      'uniqueId' => $product->uniqueId,
      'coverImage' => $product->coverImage,
      'genre' => $product->Genre->name,
      'location' => $product->Geo->name,
      'language' => $product->Language->name,
      'creatorName' => $product->UserRole->User->name,
      'creatorId' => $product->UserRole->User->id,
      'creatorEmail' => $product->UserRole->User->email
    ];

    return [
      'productDetails' => $productDetails,
      'connections' => $connections,
      'jobs' => [
        'count' => count($jobs),
        'list' => $jobs->toArray()
      ],
      'investors' => [
        'list' => $investors,
        'count' => count($productInvestors)
      ],
      'recommendations' => [
        'list' => $recommendations,
        'count' => count($ProductRecommendations)
      ]
    ];

  }

  /**
   * @title("Create")
   * @description("Create a new product")
   * @response("Product object or Error object")
   */
  public function create() {

    $data = $this->request->getJsonRawBody();

    $product = new Product;
    $product->title  = $data->title;
    $product->shortDescription = $data->description;
    $product->summary = $data->summary;
    $product->amount = $data->budget;
    $product->userRoleId = $data->role->userRoleId;
    $product->genreId = $data->genre->id;
    $product->geoId = $data->location->id;
    $product->languageId = $data->language->id;
    $product->uniqueId = $this->productUniqueId(5);
    $product->ratings = 0;
    $product->lifecycle = 0;
    $product->coverImage = 'images/ex/th-292x204-3.jpg';


    if (!$product->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not create product.');
    }

    return $this->createItemWithOK($product, new ProductTransformer, 'product');
  }

  /**
   * @title("Update")
   * @description("Update a product")
   * @response("Product object or Error object")
   */
  public function update($product_id) {

    $product = Product::findFirstById($product_id);

    if(!$product) {

      throw new UserException(ErrorCodes::DATA_NOTFOUND, 'Could not find product.');
    }

    $data = $this->request->getJsonRawBody();

    // Prepare method is provided by \OA\Phalcon\Mvc\Model
    $product->prepare($data);

    if(!$product->save()) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not update product.');
    }

    return $this->createItemWithOK($product, new ProductTransformer, 'product');
  }

  /**
   * @title("Remove")
   * @description("Remove a product")
   * @response("Result object or Error object")
   * @responseExample({
   *     "result": "OK"
   * })
   */
  public function remove($product_id) {

    if(!Product::remove($product_id)) {

      throw new UserException(ErrorCodes::DATA_FAIL, 'Could not remove product.');
    }

    return $this->respondWithOK();
  }

  private function productUniqueId($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {

    $str = '';
    $count = strlen($charset);

    while ($length--) {
        $str .= $charset[mt_rand(0, $count-1)];
    }

    $uniqueId= time().'-'.mt_rand();

    return $str.$uniqueId;
  }

 }
