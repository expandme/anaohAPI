<?php

use Library\App\Models\Product as Product;
use Library\App\Models\ProductUserRole as ProductUserRole;
use Library\App\Models\ProductImage as ProductImage;
use Library\App\Models\UserRole as UserRole;
use Library\App\Models\User as User;
use Library\App\Models\Geo as Geo;
use Library\App\Models\Genre as Genre;
use Library\App\Models\Language as Language;
use Library\App\Models\Jobs as Jobs;
use Library\App\Models\ProductFund as ProductFund;
use Library\App\Models\ProductRecommendation as ProductRecommendation;
use \PhalconRest\Constants\ErrorCodes as ErrorCodes;
use \PhalconRest\Exceptions\UserException;
//use \Searcher\Searcher;

class SearchController extends PhalconRest\Mvc\Controller {


  public function all() {
    $query = $this->request->getQuery("q", "string");
    $scriptList = [];
    $jobList = [];
    $paramsProduct = [
      "conditions" => " title LIKE '%" . $query . "%' OR shortDescription LIKE '%" . $query . "%' OR summary LIKE '%" . $query . "%'",
    ];
    $paramsJob = [
      "conditions" => " title LIKE '%" . $query . "%' OR description LIKE '%" . $query . "%' OR summary LIKE '%" . $query . "%'",
    ];

    $products = Product::find($paramsProduct);
    $jobs = Jobs::find($paramsJob);

    foreach($products as $product) {
      $scriptList[] = [
        'id' => $product->id,
        'title' => $product->title,
        'location' => $product->Geo->name,
        'author' => $product->UserRole->User->name,
        'type' => 'script'
      ];
    }
    foreach($jobs as $job) {
      $jobList[] = [
        'id' => $job->id,
        'title' => $job->title,
        'summary'=> $job->summary,
        'type' => 'job'
      ];
    }

    return ['scripts' => $scriptList, 'jobs' => $jobList];
  }
}
