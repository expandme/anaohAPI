<?php

use Library\App\Models\User as User;
use Library\App\Models\UsernameAccount as UsernameAccount;
use Library\App\Models\UserRole as UserRole;
use Library\App\Models\Role as Role;
use Library\App\Models\Language as Language;
use Library\App\Models\Geo as Geo;
use Library\App\Models\Product as Product;
use Library\App\Models\Appointment;
use Library\App\Models\Jobs;
use Library\App\Models\JobsApplied;
use Library\App\Models\ProductFund as ProductFund;
use Library\App\Models\ProductUserRole as ProductUserRole;
use Library\App\Models\ProductRecommendation as ProductRecommendation;
use Library\App\Constants\Services as AppServices;
use Library\PhalconRest\Transformers\UserTransformer;

/**
 * @resource("User")
 */
class UserController extends \PhalconRest\Mvc\Controller
{

    /**
     * @title("Activate")
     * @description("Activate user via activation-link")
     * @response("Result object or Error object")
     * @requestExample("GET /users/activate?mailtoken=2df14qh9of4qas98fynasd9")
     * @responseExample({
     *      "result": "OK"
     *  })
     */
    public function onConstruct()
    {

        parent::onConstruct();
        $this->userService = $this->di->get(AppServices::USER_SERVICE);
    }

    public function activate()
    {

        $user = $this->userService->activate();
        return $this->createItem($user, new \UserTransformer, 'user');
    }

    /**
     * @title("Create")
     * @description("Create new user (register)")
     * @response("User object or Error object")
     */
    public function create()
    {

        $data = $this->request->getJsonRawBody();

        $user = $this->userService->register($data);

        return $this->createItem($user, new \UserTransformer, 'user');
    }

    /**
     * @title("Me")
     * @description("Get the current user")
     * @includeTypes({
     *      "accounts": "Adds accounts object to the response"
     * })
     * @requestExample("GET /users/me?include=accounts")
     */
    public function me()
    {

        $user = $this->userService->me();

        $paramsUsername = [
            'columns' => 'id, username',
            'conditions' => 'userId=?1',
            'bind' => [
                1 => $user->id
            ]
        ];

        $username = UsernameAccount::findFirst($paramsUsername);

        $output = [
            'username' => $username,
            'userDetails' => $user
        ];

        // return $this->createItem($user, new \UserTransformer, 'user');
        return $output;
    }

    /**
     * @title("Authenticate")
     * @description("Authenticate user")
     * @headers({
     *      "Authorization": "'Basic sd9u19221934y='(base64 encoded username and password or code and access_token received from google client authentication)"
     * })
     * @response("Data object or Error object")
     * @responseExample({
     *      "data": {
     *          "AuthToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...",
     *          "Expires": 1428497770000
     *      }
     *  })
     */
    public function authenticate($account)
    {

        $data = $this->request->getJsonRawBody();

        return $this->respondWithArray($this->userService->login($account, $data), 'data');
    }

    public function listUser()
    {
        $query = $this->request->getQuery("q", "string");
        $results = [];

        $params = [
            "columns" => "id, name, email",
            "conditions" => " name LIKE '%" . $query . "%' OR email LIKE '%" . $query . "%'",
            "limit" => 8
        ];

        $usersList = User::find($params);

        foreach ($usersList as $user) {
            $results[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email
            ];
        }

        return ['status' => true, 'results' => $results];
    }

    public function associatedUserRoles($user_id)
    {

        $params = [
            'columns' => 'id, roleId',
            'conditions' => 'userId=?1',
            'bind' => [
                1 => $user_id
            ]
        ];

        $user_roles = UserRole::find($params);

        $userRoleDetails = [];
        $roles = [];
        foreach ($user_roles as $user_role) {
            $role = Role::findFirstById($user_role->roleId);
            $userRoleDetails[] = [
                'userRoleId' => $user_role->id,
                'roleName' => $role->name,
            ];

            array_push($roles, $role->id);
        }

        return ['userRoleDetails' => $userRoleDetails, 'associatedRoles' => $roles];
    }

    public function associatedUserProduct()
    {

        $user = $this->userService->me();

        $params = [
            'columns' => 'id',
            'conditions' => 'userId=?1',
            'bind' => [
                1 => $user->id
            ]
        ];

        $user_roles = UserRole::find($params);

        $output = [];

        foreach ($user_roles as $user_role) {

            $paramsProduct = [
                'columns' => 'id, title',
                'conditions' => 'userRoleId=?1',
                'bind' => [
                    1 => $user_role->id
                ]
            ];

            $products = Product::find($paramsProduct);

            foreach ($products as $product) {
                $output[] = [
                    'id' => $product->id,
                    'title' => $product->title
                ];
            }
        }

        return $output;
    }

    public function authorDashboard()
    {

        $output = [];
        $scriptConnection = [];
        $appointments = [];
        $repliedJobs = [];
        $user = $this->userService->me();

        $associatedRoles = $this->associatedUserRoles($user->id);

        if (in_array(6, $associatedRoles['associatedRoles'])) {
            $results = $this->getAppointmentsAndRepliedJobs($associatedRoles);
            $appointments = $results['appointments'];
            $repliedJobs = $results['repliedJobs'];
        }


        $params = [
            'conditions' => 'userId=?1',
            'bind' => [
                1 => $user->id
            ]
        ];

        $connections = ProductUserRole::find($params);

        foreach ($connections as $connection) {
            $scriptConnection[] = [
                'permissionStatus' => $connection->permission,
                'scriptId' => $connection->Product->id,
                'authorId' => $connection->Product->UserRole->userId,
                'title' => $connection->Product->title,
                'lifecycle' => $connection->Product->lifecycle,
                'coverImage' => $connection->Product->coverImage,
                'location' => $connection->Product->Geo->name,
            ];
        }

        foreach ($associatedRoles['userRoleDetails'] as $user_role) {
            $paramsProduct = [
                'columns' => 'id, title , summary, coverImage, lifecycle',
                'conditions' => 'userRoleId=?1',
                'bind' => [
                    1 => $user_role['userRoleId']
                ]
            ];

            $products = Product::find($paramsProduct);
            foreach ($products as $product) {
                $paramsInvestors = [
                    'columns' => 'id',
                    'conditions' => 'productId=?1',
                    'bind' => [
                        1 => $product->id
                    ]
                ];

                $paramsRecommendation = [
                    'columns' => 'id',
                    'conditions' => 'productId=?1',
                    'bind' => [
                        1 => $product->id
                    ]
                ];

                $paramsConnection = [
                    'columns' => 'id',
                    'conditions' => 'productId=?1 AND permission=1',
                    'bind' => [
                        1 => $product->id
                    ]
                ];

                $investors = ProductFund::find($paramsInvestors);
                $recommendation = ProductRecommendation::find($paramsRecommendation);
                $productConnections = ProductUserRole::find($paramsConnection);
                $output[] = [
                    'productId' => $product->id,
                    'title' => $product->title,
                    'summary' => $product->summary,
                    'coverImage' => $product->coverImage,
                    'lifecycle' => $product->lifecycle,
                    'investors' => count($investors),
                    'recommendations' => count($recommendation),
                    'connections' => count($productConnections),
                ];
            }
        }

        return ['userRoles' => $associatedRoles, 'details' => $output, 'connectedScripts' => $scriptConnection, 'appointments' => $appointments, 'jobsReplied' => $repliedJobs];
    }

    public function manageScript($user_id, $script_id)
    {
        $appointments = [];
        $repliedJobs = [];

        $user = $this->userService->me();

        //check if user is connected to that script or not for security with permission
        $params = [
            'conditions' => 'userId = ?1 AND productId = ?2 AND permission = 1',
            'bind' => [
                1 => $user->id,
                2 => $script_id,
            ]
        ];

        $productUserRole = ProductUserRole::findFirst($params);
        //print_r(count($productUserRole));

        //check if user has communicator as a role or not
        $associatedRoles = $this->associatedUserRoles($user->id);
        $associatedRolesAuthor = $this->associatedUserRoles($user_id);
        if (in_array(6, $associatedRoles['associatedRoles']) && count($productUserRole) > 0) {
            $results = $this->getAppointmentsAndRepliedJobs($associatedRolesAuthor, $script_id);
            $appointments = $results['appointments'];
            $repliedJobs = $results['repliedJobs'];
            return ['status' => true, 'appointments' => $appointments, 'jobsReplied' => $repliedJobs];

        } else {
            return ['status' => false, 'message' => 'some error occurred'];

        }
    }


    private function getAppointmentsAndRepliedJobs($associatedRoles, $productId = NULL)
    {
        $appointments = [];
        $repliedJobs = [];
        if ($productId != NULL) {
            $params = [
                'conditions' => 'productId=?1',
                'bind' => [
                    1 => $productId
                ]
            ];

            $repliedJobList = JobsApplied::find($params);
            $appointmentList = Appointment::find($params);
            foreach ($appointmentList as $appointment) {
                $appointments[] = [
                    'productId' => $appointment->Product->id,
                    'productTitle' => $appointment->Product->title,
                    'userName' => $appointment->User->name,
                    'userEmail' => $appointment->User->email,
                    'userId' => $appointment->User->id,
                    'appointmentDate' => $appointment->date,
                    'appointmentBookedOn' => date('M j Y', strtotime($appointment->createdAt))
                ];
            }
            foreach ($repliedJobList as $job) {
                $repliedJobs[] = [
                    'jobId' => $job->Jobs->id,
                    'jobTitle' => $job->Jobs->title,
                    'repliedUserId' => $job->User->id,
                    'repliedUserName' => $job->User->name,
                    'productId' => $job->productId,
                    'productTitle' => $job->Product->title,
                    'details' => $job->resume,
                    'appliedOn' => date('M j Y', strtotime($job->createdAt))
                ];
            }
        } else {
            foreach ($associatedRoles['userRoleDetails'] as $userRole) {

                $paramsProductList = [
                    'columns' => 'id',
                    'conditions' => 'userRoleId=?1',
                    'bind' => [
                        1 => $userRole['userRoleId']
                    ]
                ];
                $productList = Product::find($paramsProductList);
                foreach ($productList as $product) {
                    $params = [
                        'conditions' => 'productId=?1',
                        'bind' => [
                            1 => $product->id
                        ]
                    ];

                    $repliedJobList = JobsApplied::find($params);
                    $appointmentList = Appointment::find($params);
                    foreach ($appointmentList as $appointment) {
                        $appointments[] = [
                            'productId' => $appointment->Product->id,
                            'productTitle' => $appointment->Product->title,
                            'userName' => $appointment->User->name,
                            'userEmail' => $appointment->User->email,
                            'userId' => $appointment->User->id,
                            'appointmentDate' => $appointment->date,
                            'appointmentBookedOn' => date('M j Y', strtotime($appointment->createdAt))
                        ];
                    }
                    foreach ($repliedJobList as $job) {
                        $repliedJobs[] = [
                            'jobId' => $job->Jobs->id,
                            'jobTitle' => $job->Jobs->title,
                            'repliedUserId' => $job->User->id,
                            'repliedUserName' => $job->User->name,
                            'productId' => $job->productId,
                            'productTitle' => $job->Product->title,
                            'details' => $job->resume,
                            'appliedOn' => date('M j Y', strtotime($job->createdAt))
                        ];
                    }

                }

            }
        }

        return ['appointments' => $appointments, 'repliedJobs' => $repliedJobs];
    }
}
